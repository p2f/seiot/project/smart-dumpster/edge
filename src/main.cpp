#define WM_ASYNC

#include <Arduino.h>
#include <AsyncWiFiManager.h>
#include <Thing.h>
#include <WebThingAdapter.h>

#include "LedImpl.h"
#include "PotentiometerImpl.h"

#define PIN_LED_AVAIL 4
#define PIN_LED_NOT_AVAIL 5
#define PIN_POT A0
#define POT_MIN 1
#define POT_MAX 10

Led* ledAvail;
Led* ledNotAvail;
Potentiometer* pot;
AsyncWiFiManager wifiManager;

WebThingAdapter *edge;
const char *ledTypes[] = {"OnOffSwitch", "Light", nullptr};
ThingDevice wotLedAvail("ledavailable", "Led Available", ledTypes);
ThingDevice wotLedNotAvail("lednotavailable", "Led Not Available", ledTypes);
ThingProperty ledAvailOn("on", "Whether the LED is turned on", BOOLEAN,
                     "OnOffProperty");
ThingProperty ledNotAvailOn("on", "Whether the LED is turned on", BOOLEAN,
                     "OnOffProperty");
const char *potTypes[] = {"MultiLevelSensor", "Sensor", nullptr};
ThingDevice wotPot("potentiometer", "Potentiometer", potTypes);
ThingProperty potValue("weight", "Trash weight", INTEGER, "WeightProperty");

void setup() {
  Serial.begin(9600);  
  while (!wifiManager.autoConnect("P2F-IOT", "P2F-IOT"))
  {
      Serial.println("Connection to AP failed, retrying...");
  }
  Serial.println("Connection to AP succeed!");
  pinMode(PIN_LED_AVAIL, OUTPUT);
  ledAvail = new LedImpl(PIN_LED_AVAIL);
  pinMode(PIN_LED_NOT_AVAIL, OUTPUT);
  ledNotAvail = new LedImpl(PIN_LED_NOT_AVAIL);
  pinMode(PIN_POT, INPUT);
  pot = new PotentiometerImpl(PIN_POT, POT_MIN, POT_MAX);

  edge = new WebThingAdapter("smart-dumpster", WiFi.localIP());
  wotLedAvail.description = "Led available";
  wotLedNotAvail.description = "Led not available";
  wotPot.description = "Trash scale";
  ledAvailOn.title = "On/Off";
  ledNotAvailOn.title = "On/Off";
  potValue.title = "Weight amount";
  wotLedAvail.addProperty(&ledAvailOn);
  wotLedNotAvail.addProperty(&ledNotAvailOn);
  wotPot.addProperty(&potValue);
  edge->addDevice(&wotLedAvail);
  edge->addDevice(&wotLedNotAvail);
  edge->addDevice(&wotPot);
  edge->begin();

  ThingPropertyValue initialOn = {.boolean = true};
  ThingPropertyValue initialOff = {.boolean = false};
  ThingPropertyValue initialWeight = {.integer = pot->read()};
  ledAvailOn.setValue(initialOff);
  ledNotAvailOn.setValue(initialOn);
  potValue.setValue(initialWeight);
  (void)ledAvailOn.changedValueOrNull();
  (void)ledNotAvailOn.changedValueOrNull();
  (void)potValue.changedValueOrNull();
  ledAvail->switchOn();
  ledNotAvail->switchOff();
}

void loop() {   
  edge->update();
  bool onAvail = ledAvailOn.getValue().boolean;
  if (onAvail) {
    ledAvail->switchOn();
  } else {
    ledAvail->switchOff();
  }
  bool onNotAvail = ledNotAvailOn.getValue().boolean;
  if (onNotAvail) {
    ledNotAvail->switchOn();
  } else {
    ledNotAvail->switchOff();
  }
  ThingPropertyValue readWeight = {.integer = pot->read()};
  potValue.setValue(readWeight);
  if (WiFi.status() != WL_CONNECTED) {
      wifiManager.autoConnect("P2F-IOT", "P2F-IOT");
  }
}
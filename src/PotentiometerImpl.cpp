/*
 * IoT 2019/2020
 * Assignment #3: Smart Dumpster
 * Authors:
 *  - Margotta Fabrizio
 *  - Mazzini Pietro
 *  - Righetti Franco
 */

#include "PotentiometerImpl.h"

PotentiometerImpl::PotentiometerImpl(unsigned pin, unsigned min, unsigned max) {
	pinMode(pin, INPUT);
	this->pin = pin;
	this->min = min;
	this->max = max;
}

unsigned
PotentiometerImpl::read() {
	unsigned valueRead = analogRead(pin);
	unsigned int range = 1024 / (this->max - this->min);
	valueRead = valueRead / range + this->min;
	return valueRead;
}
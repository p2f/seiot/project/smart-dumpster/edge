/*
 * IoT 2019/2020
 * Assignment #3: Smart Dumpster
 * Authors:
 *  - Margotta Fabrizio
 *  - Mazzini Pietro
 *  - Righetti Franco
 */

#ifndef POTENTIOMETERIMPL_H
#define POTENTIOMETERIMPL_H

#include "Potentiometer.h"
#include <Arduino.h>

/**
 * Represents a potentiometer connected to a pin.
 */
class PotentiometerImpl : public Potentiometer {
	unsigned pin;
	unsigned min;
	unsigned max;

  public:
	/**
	 * Instanciate a potentiometer.
	 * @param pin the pin connected to the potentiometer.
	 */
	PotentiometerImpl(unsigned pin, unsigned min, unsigned max);
	unsigned read() override;
};

#endif
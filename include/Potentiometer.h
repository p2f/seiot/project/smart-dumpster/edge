/*
 * IoT 2019/2020
 * Assignment #3: Smart Dumpster
 * Authors:
 *  - Margotta Fabrizio
 *  - Mazzini Pietro
 *  - Righetti Franco
 */

#ifndef POTENTIOMETER_H
#define POTENTIOMETER_H

/**
 * Represents a potentiometer.
 */
class Potentiometer {
  public:
	/**
	 * Read the value sent by the potentiometer.
	 */
	virtual unsigned read() = 0;
};

#endif
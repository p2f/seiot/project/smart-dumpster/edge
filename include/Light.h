/*
 * IoT 2019/2020
 * Assignment #3: Smart Dumpster
 * Authors:
 *  - Margotta Fabrizio
 *  - Mazzini Pietro
 *  - Righetti Franco
 */

#ifndef LIGHT_H
#define LIGHT_H

/**
 * Represents a light source.
 */
class Light {
  public:
	/**
	 * Turn on the light source.
	 */
	virtual void switchOn() = 0;

	/**
	 * Turn off the light source.
	 */
	virtual void switchOff() = 0;

	/**
	 * Know if the light source is turned on.
	 * @return the state of the light source.
	 */
	virtual bool isTurnedOn() = 0;
};

#endif
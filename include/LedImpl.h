/*
 * IoT 2019/2020
 * Assignment #3: Smart Dumpster
 * Authors:
 *  - Margotta Fabrizio
 *  - Mazzini Pietro
 *  - Righetti Franco
 */

#ifndef LEDIMPL_H
#define LEDIMPL_H

#include "Led.h"
#include <Arduino.h>

/**
 * Represents a LED connected to a pin.
 */
class LedImpl : public Led {
	unsigned pin;
	bool isOn;

  public:
	/**
	 * Instanciates the LED.
	 * @param pin The pin in which the LED is connected.
	 */
	explicit LedImpl(unsigned pin);

	void switchOn() override;
	void switchOff() override;
	bool isTurnedOn() override;
};

#endif
